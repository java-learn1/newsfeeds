# News Feeds

Desde casa de Gobierno se quiere estar super informado sobre la pandemia del coronavirus
para poder tomar las mejores decisiones para el país.

## Desafíos:
- #### Desafío 1:
Es necesario tener las noticias actualizadas, para ello debemos guardar en una base local
todas las noticias con los campos que creas necesarios.
Inicialmente se consulta a la BD local preguntando si existen esas noticias para esa fecha,
si no existen vamos a la API de noticias y guardamos las noticias localmente para “no
agotar” la cuota del servicio externo.
- #### Desafío 2:
La búsqueda de noticias será utilizando filtros por ej.: (fuente, título, fecha)
También poder ordenar por el ranking más confiable y poder paginar los resultados.

## Solución
Crear una API Rest en Spring Boot como tecnología de base, sumando los siguiente puntos:

1. Mongo DB
2. Hateoas Pageable
3. Facade API Externa
4. Swagger doc
5. Security 

## UML diagrama
![Screenshot](UML-News.jpg)

## Endpoint
- News [(Endpoint News)](http://159.65.38.100:9897/api/v1/news)
- Doc [(Doc API News)](http://159.65.38.100:9897/swagger-ui.html)

## Ejemplos de consulta

- Pageable          [[/api/v1/news]](http://159.65.38.100:9897/api/v1/news)
- Pageable config   [[/api/v1/news?page=0&size=20]](http://159.65.38.100:9897/api/v1/news?page=0&size=20) 
- Sort              [[/api/v1/news?sort=published,desc]](http://159.65.38.100:9897/api/v1/news?sort=published,desc)
- Pageable config, sort [[/api/v1/news?page=0&size=20&sort=published,desc]](http://159.65.38.100:9897/api/v1/news?page=0&size=20&sort=published,desc)
- Pageable config, sort, query param (q) [[/api/v1/news?q=casos%20nuevos&page=0&size=20&sort=published,desc]](http://159.65.38.100:9897/api/v1/news?q=casos%20nuevos&page=0&size=20&sort=published,desc)
 



package com.md.newsfeed.service;

import com.md.newsfeed.external.model.ExternalNewDto;
import com.md.newsfeed.model.NewModel;
import com.md.newsfeed.model.dto.NewDto;
import com.md.newsfeed.model.nosql.NewDocument;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface NewService {
    PagedModel<NewModel> findAllNewsPaged(Pageable pageable, Map<String, String> allParams);
    Optional<NewDocument> findById(String id);
    List<NewDocument> saveExtenalData(List<ExternalNewDto> externalNewDtos);
}

package com.md.newsfeed.service;

import com.md.newsfeed.assembler.NewModelAssembler;
import com.md.newsfeed.external.facade.ExternalDataAPI;
import com.md.newsfeed.external.model.ExternalNewDto;
import com.md.newsfeed.external.service.ExternalDataService;
import com.md.newsfeed.model.NewModel;
import com.md.newsfeed.model.dto.NewDto;
import com.md.newsfeed.model.nosql.NewDocument;
import com.md.newsfeed.model.nosql.QNewDocument;
import com.md.newsfeed.repository.NewNoSqlRepository;
import com.querydsl.core.types.Predicate;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NewServiceImp implements NewService{
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalDataAPI.class);
    private final int updateHour;
    private static final String PARAM_Q = "q"; //Identifier query in GET Method.-
    private static final String PARAM_SITE = "site";
    private static final String PARAM_DATE = "date";
    private static final String PARAM_BEF_DATE = "bef_date";
    private static final String PARAM_AFT_DATE = "aft_date";

    @Autowired
    NewNoSqlRepository newNoSqlRepository;

    @Autowired
    ExternalDataService externalDataService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    NewModelAssembler newModelAssembler;

    @Autowired
    private PagedResourcesAssembler<NewDocument> pagedResourcesAssembler;

    private static List<String> allParamAccept;

    public NewServiceImp(@Value("${update_hour}") String updateHour){
        this.updateHour = Integer.parseInt(updateHour);
        allParamAccept = List.of(PARAM_Q, PARAM_SITE, PARAM_DATE, PARAM_BEF_DATE, PARAM_AFT_DATE);
    }

    /**
     * Logic in the search for paged news..
     * @param pageable Pageable object
     * @param queryParams Map of query params.
     * @return PagedModel of NewModel.
     */
    @Override
    public PagedModel<NewModel> findAllNewsPaged(Pageable pageable, Map<String, String> queryParams) {
        //Consulto si en la ultima hora registre datos de APIS externas.
        long countUpdate = getCountNewsUpdate();
        if (countUpdate==0){
            //Si no hay se registraron nuevas noticias, actualizo.
            List<ExternalNewDto> externalNewsDto = externalDataService.findExternalData();
            //Las noticias que se obtiene de fuentes externas
            //se procede a insertar en nuestra fuente de datos.
            saveExtenalData(externalNewsDto);
        }

        Page<NewDocument> newsDocument = findNewsDocument(pageable, queryParams);

        return pagedResourcesAssembler
                .toModel(newsDocument, newModelAssembler);
    }


    /**
     * Find by id New document.
     * @param id String id
     * @return Optional NewDocument
     */
    @Override
    public Optional<NewDocument> findById(String id) {
        return newNoSqlRepository.findById(id);
    }

    @Override
    public List<NewDocument> saveExtenalData(List<ExternalNewDto> externalNewDtos) {
        //Lógica para guardar news external.
        List<NewDocument> newDocuments = externalNewDtos.stream().map(this::converToDocument).collect(Collectors.toList());
        Iterable<NewDocument> persistedNewDocuments = newNoSqlRepository.saveAll(newDocuments);

        LOGGER.info("save external data.");
        return newDocuments;
    }

    /**
     * Search by defect or queryParams.
     * @param pageable Pageable Object
     * @param queryParams Map queryParams.
     * @return Page NewDocument
     */
    private Page<NewDocument> findNewsDocument(Pageable pageable, Map<String, String> queryParams) {
        Page<NewDocument> newsDocument;

        //Si existen Param, verifica que sean correctos.
        if (isCorrectParams(queryParams)){
            //Busqueda por params.
            Predicate queryP = getQueryPredicate(queryParams);
            newsDocument = newNoSqlRepository.findAll(queryP, pageable);
        } else{
            //Por default realiza una busqueda con date Actual.
            Predicate dateActualP = getDateActualPredicate();
            newsDocument = newNoSqlRepository.findAll(dateActualP, pageable);
        }

        return newsDocument;
    }

    /**
     * Check if there is less than one correct query.
     * @param queryParams Map query params
     * @return boolean isCorrect
     */
    private boolean isCorrectParams(Map<String, String> queryParams) {
        boolean isCorrect= false;

        if(queryParams.size()>0){
            isCorrect = queryParams.keySet().stream().anyMatch(param-> allParamAccept.contains(param));
        }
        return isCorrect;
    }

    /**
     * Get count news update.
     * @return long count News update
     */
    private long getCountNewsUpdate() {
        long countByDate;
        Predicate hourActualP =getHourActualPredicate();
        countByDate = newNoSqlRepository.count(hourActualP);
        return countByDate;
    }

    /**
     * Predicate hour actual
     * @return Predicate HourActual
     */
    private Predicate getHourActualPredicate(){
        QNewDocument newQ = new QNewDocument("new");
        return newQ.created.between(LocalDateTime.now().minusHours(updateHour),LocalDateTime.now());
    }

    /**
     * Predicate date actual
     * @return Predicate DateActual
     */
    private Predicate getDateActualPredicate(){
        QNewDocument newQ = new QNewDocument("new");
        return newQ.created.between(LocalDateTime.now().toLocalDate().atStartOfDay(), LocalDateTime.now());
    }

    /**
     * Predicate Search News document.
     * @param queryParams Map param query
     * @return PredicateSearch
     */
    private Predicate getQueryPredicate(Map<String, String> queryParams){
        Predicate predicateSearch = null;

        Predicate predicateQ = null;
        Predicate predicateSite = null;
        Predicate predicateDate = null;

        //Process allParams - Obtener los param valid
        String q = queryParams.get("q");
        String site = queryParams.get("site");
        String date = queryParams.get("date");

        QNewDocument qNew = new QNewDocument("new");
        if(site != null){
            predicateSite = qNew.thread.site.contains(site);
        }

        if(q != null){
            Predicate predicateTitle = qNew.title.contains(q);
            predicateQ = qNew.text.contains(q).or(predicateTitle);
        }

        try {
            if(date != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date dateQuery = dateFormat.parse(date);
                predicateDate = qNew.published.eq(dateQuery);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (predicateQ != null && predicateSite != null && predicateDate != null){
            predicateSearch = qNew.thread.site.contains(site).and(predicateQ).and(predicateDate);
        } else if(predicateQ != null){
            predicateSearch = predicateQ;
        } else if (predicateSite != null){
            predicateSearch = predicateSite;
        } else if (predicateDate != null){
            predicateSearch = predicateDate;
        }

        return predicateSearch;
    }

    /**
     *
     * @param externalNewDto External New Dto
     * @return NewDocument result.
     */
    private NewDocument converToDocument(ExternalNewDto externalNewDto){
        return modelMapper.map(externalNewDto, NewDocument.class);
    }

    /**
     * Convert NewDocument a NewDto
     * @param newDocument NewDocument to convert
     * @return NewDto result.
     */
    private NewDto convertToDto(NewDocument newDocument){
        return modelMapper.map(newDocument, NewDto.class);
    }

    /**
     * Scheduled Task clean db.
     */
    @Scheduled(fixedRateString = "${scheduled.task.cleardb}", initialDelayString = "${scheduled.task.delaycleardb}")
    public void clearNewsDB (){
        LOGGER.info("Info Clear News DB");
        Date date = new Date();
        QNewDocument qNew = new QNewDocument("new");
        Predicate newsDeleteP = qNew.published.before(date);
        Iterable<NewDocument> newsDocumentDelete = newNoSqlRepository.findAll(newsDeleteP);

        newNoSqlRepository.deleteAll(newsDocumentDelete);
    }
}

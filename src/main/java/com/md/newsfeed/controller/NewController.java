package com.md.newsfeed.controller;

import com.md.newsfeed.assembler.NewModelAssembler;
import com.md.newsfeed.model.NewModel;
import com.md.newsfeed.model.nosql.NewDocument;
import com.md.newsfeed.repository.NewNoSqlRepository;
import com.md.newsfeed.service.NewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/news")
public class NewController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewController.class);

    @Autowired
    NewNoSqlRepository newNoSqlRepository;

    @Autowired
    NewService newService;

    @Autowired
    NewModelAssembler newModelAssembler;

    /**
     * Pageable data news document converted to NewModel paged.
     * @param pageable Pageable
     * @param query All query param
     * @return PagedModel of NewModel data. (DTO)
     */
    @GetMapping
    public ResponseEntity<PagedModel<NewModel>> getAllNewsPaged(Pageable pageable, @RequestParam(required = false) Map<String,String> query){
        PagedModel<NewModel> collNewsModel = newService.findAllNewsPaged(pageable, query);

        if(collNewsModel.getContent().isEmpty()){
            LOGGER.info("getAllNewsPaged, news empty.");
            return ResponseEntity.noContent().build();
        }

        LOGGER.info("getAllNewsPaged, news ok.");
        return ResponseEntity.ok(collNewsModel);
    }

    @GetMapping("/{id}")
    public ResponseEntity<NewDocument> getNewById(@PathVariable("id") String id){
        Optional<NewDocument> newDocumentOptional = newService.findById(id);
        return newDocumentOptional
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/all")
    public ResponseEntity<CollectionModel<NewModel>> getAllNews(){
        List<NewDocument> newDocuments = newNoSqlRepository.findAll();

        return new ResponseEntity<>(
                newModelAssembler.toCollectionModel(newDocuments),
                HttpStatus.OK);
    }
}

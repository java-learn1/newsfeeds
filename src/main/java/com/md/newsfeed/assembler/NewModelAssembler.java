package com.md.newsfeed.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.md.newsfeed.model.NewModel;
import com.md.newsfeed.model.nosql.NewDocument;
import com.md.newsfeed.controller.NewController;

@Component
public class NewModelAssembler
        extends RepresentationModelAssemblerSupport<NewDocument, NewModel> {

    private static final int LENGTH_TWITEER = 140;
    private static final String READ_MORE = "... (leer más)";

    public NewModelAssembler() {
        super(NewController.class, NewModel.class);
    }

    @Override
    public NewModel toModel(NewDocument document){
        NewModel newModel = instantiateModel(document);

        newModel.add(linkTo(
                methodOn(NewController.class)
                        .getNewById(document.get_id()))
                .withSelfRel());

        newModel.setId(document.get_id());
        newModel.setTitle(document.getTitle());
        int lengthSummary = (document.getText().length()>=LENGTH_TWITEER+1)?LENGTH_TWITEER:document.getText().length();
        newModel.setSummaryText(document.getText().substring(0, lengthSummary).concat(READ_MORE));
        newModel.setUrl(document.getUrl());
        newModel.setPublished(document.getPublished());

        return newModel;
    }

    @Override
    public CollectionModel<NewModel> toCollectionModel(Iterable<? extends NewDocument> documents){
        CollectionModel<NewModel> newModels = super.toCollectionModel(documents);

        newModels.add(linkTo(methodOn(NewController.class).getAllNews()).withSelfRel());

        return newModels;
    }
}

package com.md.newsfeed.repository;

import com.md.newsfeed.model.nosql.NewDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NewNoSqlRepository
        extends MongoRepository<NewDocument, String>,
        QuerydslPredicateExecutor<NewDocument>,
        PagingAndSortingRepository<NewDocument, String> {
}

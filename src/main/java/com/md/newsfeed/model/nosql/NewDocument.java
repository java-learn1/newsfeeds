package com.md.newsfeed.model.nosql;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.md.newsfeed.external.model.webhose.thread;
import com.querydsl.core.annotations.QueryEntity;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;

@QueryEntity
@Document(collection = "new")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewDocument {
    @Id
    public ObjectId _id;
    private thread thread;
    private String uuid;
    private String author;
    private String url;
    private String title;
    private String text;
    private Date published;
    private Date crawled;
    private String rating;

    private LocalDateTime created;

    public NewDocument() {
        this.created = LocalDateTime.now();
    }

    // ObjectId needs to be converted to string
    public String get_id() { return _id.toHexString(); }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public Date getCrawled() {
        return crawled;
    }

    public void setCrawled(Date crawled) {
        this.crawled = crawled;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public com.md.newsfeed.external.model.webhose.thread getThread() {
        return thread;
    }

    public void setThread(com.md.newsfeed.external.model.webhose.thread thread) {
        this.thread = thread;
    }
}

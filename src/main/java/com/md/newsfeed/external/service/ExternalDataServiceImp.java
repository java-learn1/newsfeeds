package com.md.newsfeed.external.service;

import com.md.newsfeed.external.facade.ExternalDataAPIFacade;
import com.md.newsfeed.external.model.ExternalNewDto;
import com.md.newsfeed.model.nosql.NewDocument;
import com.md.newsfeed.service.NewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ExternalDataServiceImp implements ExternalDataService{
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalDataServiceImp.class);

    @Autowired
    NewService newService;

    ExternalDataAPIFacade externalDataAPIFacade;

    public ExternalDataServiceImp (RestTemplate restTemplate){
        this.externalDataAPIFacade = new ExternalDataAPIFacade(restTemplate);
    }

    @Override
    public List<ExternalNewDto> findExternalData() {
        LOGGER.info("Get external data IMP");
        List<ExternalNewDto> externalNewsDto = externalDataAPIFacade.getData();
        return externalNewsDto;
    }

    @Override
    public void saveExternalData(List<ExternalNewDto> externalNewDtos) {
        newService.saveExtenalData(externalNewDtos);
    }

}

package com.md.newsfeed.external.service;

import com.md.newsfeed.external.model.ExternalNewDto;
import com.md.newsfeed.model.nosql.NewDocument;

import java.util.List;

public interface ExternalDataService {
    List<ExternalNewDto> findExternalData();
    void saveExternalData(List<ExternalNewDto> externalNewDtos);
}

package com.md.newsfeed.external.model.webhose;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class thread {
    String site;
    String country;
    String domain_rank;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDomain_rank() {
        return domain_rank;
    }

    public void setDomain_rank(String domain_rank) {
        this.domain_rank = domain_rank;
    }
}

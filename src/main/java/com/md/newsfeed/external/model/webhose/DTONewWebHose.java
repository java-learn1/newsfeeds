package com.md.newsfeed.external.model.webhose;

import com.md.newsfeed.external.model.ExternalNewDto;

import java.util.Date;

public class DTONewWebHose extends ExternalNewDto {
    private thread thread;
    private String author;
    private String url;
    private String title;
    private String text;
    private Date published;
    private Date crawled;
    private String rating;

    public com.md.newsfeed.external.model.webhose.thread getThread() {
        return thread;
    }

    public void setThread(com.md.newsfeed.external.model.webhose.thread thread) {
        this.thread = thread;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public Date getCrawled() {
        return crawled;
    }

    public void setCrawled(Date crawled) {
        this.crawled = crawled;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}

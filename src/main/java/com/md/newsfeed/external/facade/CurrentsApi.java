package com.md.newsfeed.external.facade;

import com.md.newsfeed.external.model.ExternalNewDto;

import java.util.List;

public class CurrentsApi extends ExternalDataAPI {
    static final String URL = "https://api.currentsapi.services/v1/search?&keywords=covid-19&language=es&apiKey=9oab3WKxI13kLg3sY5P-r_LSwX0-g7fPqpzlMFAP5UBoPz5M";

    @Override
    public List<ExternalNewDto> processData(String responseData) {
        return null;
    }

    @Override
    public String urlAPI() {
        return URL;
    }
}

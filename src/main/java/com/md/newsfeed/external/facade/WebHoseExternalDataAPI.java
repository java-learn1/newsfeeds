package com.md.newsfeed.external.facade;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.md.newsfeed.external.model.ExternalNewDto;
import com.md.newsfeed.external.model.webhose.DTONewWebHose;
import com.md.newsfeed.external.util.ResponseWebHose;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WebHoseExternalDataAPI extends ExternalDataAPI{

    private static final Logger LOGGER = LoggerFactory.getLogger(WebHoseExternalDataAPI.class);

    //?token=86faad70-82e9-45cb-b381-d14614b31cbd&format=json&ts=1595661107020&q=coronavirus+casos+positivos++language%3Aspanish+thread.country%3AAR&sort=crawled
    //?token=86faad70-82e9-45cb-b381-d14614b31cbd&format=json&sort=crawled&q=coronavirus%20casos%20positivos%20%20language%3Aspanish%20thread.country%3AAR

    //http://webhose.io/filterWebContent?token=86faad70-82e9-45cb-b381-d14614b31cbd&format=json&sort=crawled&q=coronavirus%20casos%20positivos%20%20language%3Aspanish%20thread.country%3AAR

    static final String URL = "http://webhose.io/filterWebContent?token=86faad70-82e9-45cb-b381-d14614b31cbd&format=json&sort=crawled&q=coronavirus+casos+positivos++language%3Aspanish+thread.country%3AAR&from=100";

    @Override
    public List<ExternalNewDto> processData(String responseBody) {
        List<ExternalNewDto> responsesNews = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            JsonNode responseObj = objectMapper.readTree(responseBody);
            List<DTONewWebHose> listNews = objectMapper.readValue(responseObj.get("posts").toString(), new TypeReference<List<DTONewWebHose>>(){});

            responsesNews = new ArrayList<>();
            for (DTONewWebHose newWebHose : listNews) {
                responsesNews.add((ExternalNewDto)newWebHose);
            }

            LOGGER.info("processData exit!");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responsesNews;
    }

    @Override
    public String urlAPI() {
        return URL;
    }
}

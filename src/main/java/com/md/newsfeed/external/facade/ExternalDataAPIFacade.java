package com.md.newsfeed.external.facade;

import com.md.newsfeed.external.model.ExternalNewDto;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExternalDataAPIFacade {

    RestTemplate restTemplate;
    private final List<ExternalDataAPI> externalsDataAPI;

    public ExternalDataAPIFacade(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        externalsDataAPI = List.of(
                new WebHoseExternalDataAPI());
    }

    public List<ExternalNewDto> getData(){
        List<ExternalNewDto> externalNewDtoList = actionGetData(restTemplate, externalsDataAPI);
        return externalNewDtoList;
    }

    public List<ExternalNewDto> actionGetData(RestTemplate restTemplate, Collection<ExternalDataAPI> externalsDataAPI) {
        List<ExternalNewDto> externalDataList = new ArrayList<>();

        externalsDataAPI.forEach(externalDataAPI->{
            externalDataList.addAll(externalDataAPI.actionGet(restTemplate));
        });

        return externalDataList;
    }
}

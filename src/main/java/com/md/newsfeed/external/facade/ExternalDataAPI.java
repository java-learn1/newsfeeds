package com.md.newsfeed.external.facade;

import com.md.newsfeed.external.model.ExternalNewDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class ExternalDataAPI {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalDataAPI.class);

    public void saveData() {
        LOGGER.info("{} save data.", urlAPI());
    }

    public List<ExternalNewDto> getActionData(RestTemplate restTemplate) {
        LOGGER.info("{} get data.", urlAPI());
        List<ExternalNewDto> externalNewDtoList = new ArrayList<>();
        try {
            URL url = new URL(urlAPI());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);

            int statusCode = connection.getResponseCode();
            if(HttpStatus.OK.value() == statusCode) {
                // Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                rd.close();

                externalNewDtoList = processData(response.toString());
                LOGGER.info("Status", statusCode);
            }else {
                LOGGER.error("Failed to get data", statusCode);
            }
        } catch(Exception ex) {
            LOGGER.error("Failed to get data", ex);
        }
        return externalNewDtoList;
    }

    public List<ExternalNewDto> actionGet(RestTemplate restTemplate){
        return getActionData(restTemplate);
    }

    public abstract List<ExternalNewDto> processData(String responseData);
    public abstract String urlAPI();

}

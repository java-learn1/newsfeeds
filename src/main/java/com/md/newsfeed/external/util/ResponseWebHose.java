package com.md.newsfeed.external.util;

public class ResponseWebHose {
    String responseBody;

    public ResponseWebHose(){
        this.responseBody = "{\n" +
                "  \"posts\": [\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"2b71443770ccf7fb068fe7699f1b7b5d6e1f793c\",\n" +
                "        \"url\": \"https://www.elliberal.com.ar/noticia/535588/coronavirus-primera-vez-hubo-mas-100-muertes-jornada\",\n" +
                "        \"site_full\": \"www.elliberal.com.ar\",\n" +
                "        \"site\": \"elliberal.com.ar\",\n" +
                "        \"site_section\": \"https://feeds.feedburner.com/elliberaldigital?format=xml\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"El Liberal\",\n" +
                "        \"title\": \"Coronavirus: por primera vez hubo más de 100 muertes en una jornada\",\n" +
                "        \"title_full\": \"Coronavirus: por primera vez hubo más de 100 muertes en una jornada\",\n" +
                "        \"published\": \"2020-07-20T03:00:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 0,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://www.elliberal.com.ar/fotos/cache/notas/2020/07/20/476x248_535588_20200720203525.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": 3061,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"2b71443770ccf7fb068fe7699f1b7b5d6e1f793c\",\n" +
                "      \"url\": \"https://www.elliberal.com.ar/noticia/535588/coronavirus-primera-vez-hubo-mas-100-muertes-jornada\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"\",\n" +
                "      \"published\": \"2020-07-20T03:00:00.000+03:00\",\n" +
                "      \"title\": \"Coronavirus: por primera vez hubo más de 100 muertes en una jornada\",\n" +
                "      \"text\": \"Coronavirus: por primera vez hubo más de 100 muertes en una jornada Además, se reportaron 3.937 nuevos casos positivos. Así lo detalló el Ministerio de Salud.\\n20/07/2020 - 20:35 País\\nEl Ministerio de Salud de Nación indicó en su reporte diario que en las últimas 24 horas se confirmaron 3.937 nuevos casos de coronavirus \u200By 113 muertes.\\nEn la mañana del lunes la cartera sanitaria había informado de 21 muertes, 17 hombres y cuatro mujeres. D iez de ellos de la provincia de Buenos Aires; nueve de la ciudad de Buenos Aires, uno de Chaco y otro de Río Negro.\\nEl total de casos confirmados hasta la mañana de este lunes (49,4% mujeres y 50,6% hombres), el 0,9% eran importados, 31,7% son contactos estrechos, 50,2% de circulación comunitaria y el resto se encuentra en investigación epidemiológica. El total de altas es de 55.913 personas.\\nEl domingo se habían realizado 11.068 nuevas muestras y desde el inicio del brote fueron hechas 552.306 pruebas diagnósticas para la enfermedad, lo que equivale a 12.171,6 muestras por millón de habitantes.\\nDurante la jornada también hubo novedades a nivel mundial respecto a la carrera por e ncontrar una cura contra el virus.\\nLa vacuna que desarrolla la universidad británica de Oxford resultó “segura” y “entrena” el sistema inmunológico, según revelaron los hallazgos de las primeras fases del estudio, divulgados este lunes.\\nLa fórmula, denominada AZD1222, está siendo elaborada por AstraZeneca en colaboración con científicos de la Universidad de Oxford, y no presentó ningún efecto colateral grave en los 1.077 voluntarios, adultos sanos de entre 18 y 55 años, que produjeron respuestas inmunes de anticuerpos y células T que pueden combatir el virus, según los resultados del ensayo publicados en la revista médica The Lancet.\\nEstos descubrimientos se consideran “muy prometedores”, si bien todavía es necesario llevar a cabo ensayos a mayor escala a fin de determinar si los anticuerpos son suficientes para ofrecer protección a largo plazo contra la enfermedad.\\nDetalle por provincia (Nº de confirmados | Nº de acumulados)*:\\nBuenos Aires 2.556 | 73.340\\nCiudad de Buenos Aires 1.090 | 46.706\\nCatamarca 0 | 58\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:48:16.002+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:48:16.002+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"ef5b1bbc0e14395b42acb4fa57c68894e55375ce\",\n" +
                "        \"url\": \"http://elfuertediario.com.ar/se-confirmo-el-primer-fallecido-por-coronavirus-en-chascomus/\",\n" +
                "        \"site_full\": \"elfuertediario.com.ar\",\n" +
                "        \"site\": \"elfuertediario.com.ar\",\n" +
                "        \"site_section\": \"http://elfuertediario.com.ar/feed\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"EL FUERTE DIARIO\",\n" +
                "        \"title\": \"Se confirmó el primer fallecido por coronavirus en Chascomús\",\n" +
                "        \"title_full\": \"Se confirmó el primer fallecido por coronavirus en Chascomús\",\n" +
                "        \"published\": \"2020-07-21T03:06:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 1,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"http://elfuertediario.com.ar/wp-content/uploads/2020/07/111476966_3419772588043602_572373690916127243_n.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": null,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"ef5b1bbc0e14395b42acb4fa57c68894e55375ce\",\n" +
                "      \"url\": \"http://elfuertediario.com.ar/se-confirmo-el-primer-fallecido-por-coronavirus-en-chascomus/\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"EDITORA\",\n" +
                "      \"published\": \"2020-07-21T03:06:00.000+03:00\",\n" +
                "      \"title\": \"Se confirmó el primer fallecido por coronavirus en Chascomús\",\n" +
                "      \"text\": \"Inicio CHASCOMUS Se confirmó el primer fallecido por coronavirus en Chascomús Se confirmó el primer fallecido por coronavirus en Chascomús 21 julio, 2020 CHASCOMUS , DESTACADAS 0\\nSe informó este lunes 20 de julio desde la Municipalidad sobre el estado de situación de la pandemia del Coronavirus en nuestra ciudad, donde se confirmó el fallecimiento del primer paciente con COVID de nuestra ciudad. Se trata de un hombre de 68 años con comorbilidades, el cual se encontraba internado en el Hospital Municipal, produciéndose el deceso ayer en horas de la madrugada. Posteriormente se conoció el resultado positivo de COVID-19 sobre este paciente.\\nPor otro lado, se confirmaron otros tres casos positivos de COVID -19; tratándose de una mujer de 40 años (contacto); y otros dos hombres, de 37 años (en investigación), y de 41 años (recuperado tras recibir el alta médica luego de 10 días de aislamiento\\nA su vez, se informó que se recuperaron 2 casos, por lo cual se registran 30 casos confirmados de COVID -19; 6 están activos, 23 se encuentran recuperados, y 1 fallecido.\\nDe los 6 casos que permanecen activos; 5 realizan el aislamiento en su domicilio y 1 se encuentra en el Hospital Italiano de la ciudad de la Plata.\\nPor otro lado, se registraron 5 casos en estudio. En total suman 17 los casos que aguardan los resultados de los análisis.\\nY fueron descartados 3 casos que permanecían en estudio. En total se registran 158 casos descartados.\\nCabe indicar que ante una urgencia se pueden comunicar al 107, ante síntomas de fiebre, tos y dolor de garganta. Por cuestiones de aislamiento sin síntomas, se puede llamar al (2241) 15604864; y en caso de incumplimiento de normativas, al 911 o al 15554600.\\nY por último, se recuerda que es obligatorio el uso de tapa nariz y boca para circular, y respetar el aislamiento social. Compártelo:\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [\n" +
                "        \"http://data-vocabulary.org/Breadcrumb\",\n" +
                "        \"http://www.data-vocabulary.org/Breadcrumb\"\n" +
                "      ],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:48:38.012+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:48:38.012+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"d072aead166afda74890fa14238a71b6cc706e6a\",\n" +
                "        \"url\": \"https://www.elliberal.com.ar/noticia/535584/asi-luce-villa-atamisqui-al-cumplir-primer-dia-aislamiento\",\n" +
                "        \"site_full\": \"www.elliberal.com.ar\",\n" +
                "        \"site\": \"elliberal.com.ar\",\n" +
                "        \"site_section\": \"https://feeds.feedburner.com/elliberaldigital?format=xml\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"El Liberal\",\n" +
                "        \"title\": \"Así luce Villa Atamisqui al cumplir su primer día de aislamiento\",\n" +
                "        \"title_full\": \"Así luce Villa Atamisqui al cumplir su primer día de aislamiento\",\n" +
                "        \"published\": \"2020-07-20T03:00:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 0,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://www.elliberal.com.ar/fotos/cache/notas/2020/07/20/476x248_535584_20200720190722.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": 3061,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"d072aead166afda74890fa14238a71b6cc706e6a\",\n" +
                "      \"url\": \"https://www.elliberal.com.ar/noticia/535584/asi-luce-villa-atamisqui-al-cumplir-primer-dia-aislamiento\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"\",\n" +
                "      \"published\": \"2020-07-20T03:00:00.000+03:00\",\n" +
                "      \"title\": \"Así luce Villa Atamisqui al cumplir su primer día de aislamiento\",\n" +
                "      \"text\": \"EL LIBERAL Interior #DESOLADO Así luce Villa Atamisqui al cumplir su primer día de aislamiento Tras confirmarse un caso de coronavirus, “parece un pueblo fantasma”, como lo describe un vecino. Entrá a la nota y mirá los vídeos.\\n20/07/2020 - 19:07 Interior\\nLa forma más efectiva de controlar la propagación del coronavirus COVID-19 es reducir el desplazamiento de las personas, confían los expertos. Algo que hoy está viviendo en carne propia los vecinos de Villa Atamisqui.\\nAyer domingo, en ese lugar se confirmó un caso positivo y por ello el gobernador de la provincia, Gerardo Zamora decretó el aislamiento preventivo y así es como lucen las calles de la ciudad en su primer día de confinamiento.\\n“P arece que vivimos en un pueblo fantasma . No hay nadie en la calle. Es triste, ojalá pase rápido todo esto”, detalló a EL LIBERAL, un vecino.\\nEsta medida durará el tiempo que dure la investigación epidemiológica tanto en Villa Atamisqui como en Juanillo , según lo expresado por el mandatario.\\nCabe destacar la presencia de efectivos de la fuerza de seguridad para verificar que se cumplan con las medidas dictadas. TAGS\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:48:54.026+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:48:54.026+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"c4435b1fa17d28b2dc0fb81350cce269d42f39f0\",\n" +
                "        \"url\": \"https://www.telesurtv.net/news/brasil-supera-ochenta-mil-fallecidos-coronavirus-20200720-0056.html\",\n" +
                "        \"site_full\": \"www.telesurtv.net\",\n" +
                "        \"site\": \"telesurtv.net\",\n" +
                "        \"site_section\": \"http://www.telesurtv.net/rss/RssPortada.html\",\n" +
                "        \"site_categories\": [\n" +
                "          \"under_construction\",\n" +
                "          \"non_standard_content\"\n" +
                "        ],\n" +
                "        \"section_title\": \"Rss Portada | teleSUR\",\n" +
                "        \"title\": \"Brasil supera los 80.000 fallecidos por coronavirus\",\n" +
                "        \"title_full\": \"Brasil supera los 80.000 fallecidos por coronavirus\",\n" +
                "        \"published\": \"2020-07-20T12:05:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 1,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://www.telesurtv.net/__export/1595294269105/sites/telesur/img/2020/07/20/brasil_muertes_por_covid-19.jpg_1810791533.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": 10612,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"c4435b1fa17d28b2dc0fb81350cce269d42f39f0\",\n" +
                "      \"url\": \"https://www.telesurtv.net/news/brasil-supera-ochenta-mil-fallecidos-coronavirus-20200720-0056.html\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"teleSUR - jdo - LES\",\n" +
                "      \"published\": \"2020-07-20T12:05:00.000+03:00\",\n" +
                "      \"title\": \"Brasil supera los 80.000 fallecidos por coronavirus\",\n" +
                "      \"text\": \"Recibe nuestros boletines de noticias directamente en tu bandeja de entrada Ya me he suscrito | No volver a mostrar este mensaje. Boletines Su suscripción fue realizada satisfactoriamente.\\nEste lunes se reportaron 20.257 nuevos casos. Ya se han confirmado 2.118.646 infectados de Covid-19.\\nBrasil llegó este lunes a los 80.120 decesos a causa de la Covid-19, tras la confirmación por las autoridades sanitarias de 632 nuevos decesos provocados por la pandemia.\\nLEA TAMBIÉN:\\nMédicos brasileños desaconsejan el uso de la hidroxicloroquina\\nEl país suramericano continúa registrando una significativa cifra de contagios por día, y con los 20.257 casos positivos reportados durante la última jornada ya acumula 2.118.646 infectados desde el inicio del brote de coronavirus.\\nDe acuerdo con el balance diario del Consejo Nacional de Secretarios de Salud (Conass), esas cifras mantienen a Brasil como la segunda nación más afectada por el Sars-Cov-2 en todo el mundo, solo detrás de Estados Unidos, que tiene 3.960.583 infectados y 143.792 víctimas mortales. ❗Atualização 20/07 do #PainelConass Covid-19: Casos 20.257 casos no último período 2.118.646 casos acumulados 632 óbitos no último período 80.120 óbitos acumulados — CONASS (@ConassOficial) July 20, 2020\\nComo entidades federales más afectadas permanecen São Paulo, Ceará, Río de Janeiro, Pará, Bahía y Maranhão, todos por encima de los 100.000 contagios. Les siguen Minas Gerais y Amazonas, con más de 90.000 positivos al virus.\\nEste lunes también se conocieron los resultados de un estudio elaborado por científicos de la Universidad Federal de Pelotas (Río Grande do Sul), quienes determinaron que alrededor del 3,8 por ciento de la población brasileña ya ha sido infectada, cifra muy lejana al 60-70 por ciento de la población que, de acuerdo con otros estudios y defensores de esa tesis, se estima necesaria para crear la llamada inmunidad de rebaño o inmunidad colectiva. A vacina chinesa contra a Covid-19 já chegou ao Brasil e os testes com os voluntários devem começar nesta segunda-feira (20) em São Paulo. A vacina foi enviada ao Instituto Butantan. São doses do laboratório chinês Sinovac Biotech e chegaram nesta madrugada no aeroporto de SP. — Mídia NINJA #JulhoDasPretas (@MidiaNINJA) July 20, 2020\\nAnte la certeza de que ya han muerto más de 80.000 personas, los científicos cuestionan si sería ético dejar morir a casi un millón más y luego alcanzar ese tipo de inmunidad.\\n\\\"La idea de la orientación de la inmunidad de grupo como una política de salud es absurda, mal concebida y poco ética\\\", manifestó el epidemiólogo Pedro Hallal, coordinador del estudio y presidente de la universidad.\\nA su juicio, \\\"el mayor error fue no haber tenido nunca una política de pruebas amplia y masiva\\\", agregó el académico, para quien la respuesta y errores de su país ante la pandemia no tienen precedentes en todo el mundo. “Varias otras naciones tampoco tuvieron buenos resultados, pero no volvieron a abrir antes de que la curva cayera. Ningún otro lugar ha hecho algo tan malo\\\", señaló. Tags\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [\n" +
                "        \"https://twitter.com/hashtag/PainelConass?src=hash&ref_src=twsrc%5Etfw\",\n" +
                "        \"https://twitter.com/MidiaNINJA/status/1285230766981558272?ref_src=twsrc%5Etfw\",\n" +
                "        \"https://www.twitter.com/MidiaNINJA/status/1285230766981558272?ref_src=twsrc%5Etfw\",\n" +
                "        \"https://twitter.com/MidiaNINJA/status/1285230766981558272\",\n" +
                "        \"https://www.twitter.com/hashtag/PainelConass?src=hash&ref_src=twsrc%5Etfw\",\n" +
                "        \"https://twitter.com/ConassOficial/status/1285318589558521856\",\n" +
                "        \"https://twitter.com/hashtag/PainelConass\",\n" +
                "        \"https://twitter.com/ConassOficial/status/1285318589558521856?ref_src=twsrc%5Etfw\",\n" +
                "        \"https://www.twitter.com/ConassOficial/status/1285318589558521856?ref_src=twsrc%5Etfw\"\n" +
                "      ],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:49:31.006+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:49:31.006+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"14b0465822dd09d18e09fed9fe2811ba3d6346e8\",\n" +
                "        \"url\": \"https://www.clarin.com/sociedad/coronavirus-argentina-informan-113-muertes-3-937-nuevos-casos-ultimas-24-horas_0_pRtA8vuR6.html\",\n" +
                "        \"site_full\": \"www.clarin.com\",\n" +
                "        \"site\": \"clarin.com\",\n" +
                "        \"site_section\": \"https://www.clarin.com/\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"Noticias. Últimas noticias de Argentina y el Mundo | Clarín\",\n" +
                "        \"title\": \"Coronavirus en Argentina: informan otras 113 muertes y 3.937 nuevos casos en las últimas 24 horas\",\n" +
                "        \"title_full\": \"Coronavirus en Argentina: informan otras 113 muertes y 3.937 nuevos casos en las últimas 24 horas - Clarín\",\n" +
                "        \"published\": \"2020-07-21T04:35:00.000+03:00\",\n" +
                "        \"replies_count\": 41,\n" +
                "        \"participants_count\": 24,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0.007,\n" +
                "        \"main_image\": \"https://images.clarin.com/2020/06/20/recorrida-por-el-hospital-evita___H6stgHk_z_1200x630__1.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": 1378,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"6df36f963e62540fd45fc7a84415dddaa619b39a\",\n" +
                "      \"url\": \"https://www.clarin.com/sociedad/coronavirus-argentina-informan-113-muertes-3-937-nuevos-casos-ultimas-24-horas_0_pRtA8vuR6.html#90e4cfd4f3a14ca191cd8aea48d4e9d4\",\n" +
                "      \"ord_in_thread\": 3,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"Anto Magnoni\",\n" +
                "      \"published\": \"2020-07-21T03:57:00.000+03:00\",\n" +
                "      \"title\": \"\",\n" +
                "      \"text\": \"¿Cuándo piensan informar acerca de que los test de PCR dan un enorme porcentaje de FALSOS POSITIVOS Da positivo no sólo en casos de covid, sino también para otros coronavirus, con lo cual hay una enorme cantidad de falsos positivos.\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": null,\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:49:52.019+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:49:52.019+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"d5f17df41878b94f52d96832ac9ec45ddbe09cc0\",\n" +
                "        \"url\": \"https://www.launion.digital/mundo/se-registraron-113-muertes-coronavirus-argentina-cifra-mas-alta-una-jornada-n33852\",\n" +
                "        \"site_full\": \"www.launion.digital\",\n" +
                "        \"site\": \"launion.digital\",\n" +
                "        \"site_section\": \"http://launiondigital.com.ar/rss.xml\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"La Unión Digital\",\n" +
                "        \"title\": \"Se registraron 113 muertes por coronavirus en Argentina, la cifra más alta en una jornada\",\n" +
                "        \"title_full\": \"Se registraron 113 muertes por coronavirus en Argentina, la cifra más alta en una jornada\",\n" +
                "        \"published\": \"2020-07-21T03:50:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 0,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://statics.launion.digital/2020/07/crop/5f163d5031517__940x620.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": null,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"d5f17df41878b94f52d96832ac9ec45ddbe09cc0\",\n" +
                "      \"url\": \"https://www.launion.digital/mundo/se-registraron-113-muertes-coronavirus-argentina-cifra-mas-alta-una-jornada-n33852\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"\",\n" +
                "      \"published\": \"2020-07-21T03:50:00.000+03:00\",\n" +
                "      \"title\": \"Se registraron 113 muertes por coronavirus en Argentina, la cifra más alta en una jornada\",\n" +
                "      \"text\": \"Clasificados Se registraron 113 muertes por coronavirus en Argentina, la cifra más alta en una jornada En el primer día de la nueva fase de la cuarentena en el AMBA, se reportaron 3.937 nuevos casos positivos y 113 fallecidos. 20 Julio de 2020 21.50\\nEl Ministerio de Salud informó este lunes que en las últimas 24 horas hubo 3.937 nuevos casos de coronavirus y que fallecieron otras 113 personas. Con los últimos datos, la cantidad de contagios desde que comenzó la pandemia alcanzó los 130.774 y las víctimas fatales ascienden a 2.373.\\nDel total de esos casos, 1.095 (0,8%) son importados, 41.086 (31,4%) son contactos estrechos de casos confirmados, 66.293 (50,7%) son casos de circulación comunitaria y el resto se encuentra en investigación epidemiológica.\\nDesde el último reporte emitido, se registraron 92 nuevas muertes: 46 hombres, 30 residentes en la provincia de Buenos Aires; 14 residentes en la Ciudad de Buenos Aires (CABA); 2 residentes Río Negro; y 46 mujeres; 17 residentes en la provincia de Buenos Aires; 27 residentes en la Ciudad de Buenos Aires (CABA); 1 residente en la provincia de Mendoza; y 1 residente en la provincia de Río Negro. Al momento la cantidad de personas fallecidas es 2.373.\\nEn el parte matutino, las autoridades sanitarias ya habían reportado 21 muertes: 17 hombres, 6 de 61, 74, 86, 80, 51 y 70 años, residentes en la provincia de Buenos Aires; 9 de 86, 78, 78, 89, 61, 51, 83, 85 y 62 años, residentes en la Ciudad de Buenos Aires (CABA); uno de 82 años, residente en la provincia de Chaco; uno de 87 años, residente en la provincia de Río Negro; y 4 mujeres de 74, 61, 64 y 67 años, residentes en la provincia de Buenos Aires.\\nA la fecha, el total de altas es de 55.913 personas. Ayer fueron realizadas 11.068 nuevas muestras y desde el inicio del brote se realizaron 552.306 pruebas diagnósticas para esta enfermedad, lo que equivale a 12.171,6 muestras por millón de habitantes.\\nEl número de casos descartados hasta ayer es de 332.433 (por laboratorio y por criterio clínico/ epidemiológico). Tags\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:53:25.000+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:53:25.000+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"2cdd046b41b1289d5356c3a24aea93ce4e87edcc\",\n" +
                "        \"url\": \"https://www.launion.digital/sociedad/coronavirus-catamarca-confirmaron-dos-contagios-nuevos-tres-pacientes-recuperados-n33851\",\n" +
                "        \"site_full\": \"www.launion.digital\",\n" +
                "        \"site\": \"launion.digital\",\n" +
                "        \"site_section\": \"http://launiondigital.com.ar/rss.xml\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"La Unión Digital\",\n" +
                "        \"title\": \"Coronavirus en Catamarca: confirmaron dos contagios nuevos y tres pacientes recuperados\",\n" +
                "        \"title_full\": \"Coronavirus en Catamarca: confirmaron dos contagios nuevos y tres pacientes recuperados\",\n" +
                "        \"published\": \"2020-07-21T03:05:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 0,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://statics.launion.digital/2020/07/crop/5f15d65e66c43__940x620.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": null,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"2cdd046b41b1289d5356c3a24aea93ce4e87edcc\",\n" +
                "      \"url\": \"https://www.launion.digital/sociedad/coronavirus-catamarca-confirmaron-dos-contagios-nuevos-tres-pacientes-recuperados-n33851\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"\",\n" +
                "      \"published\": \"2020-07-21T03:05:00.000+03:00\",\n" +
                "      \"title\": \"Coronavirus en Catamarca: confirmaron dos contagios nuevos y tres pacientes recuperados\",\n" +
                "      \"text\": \"Desde el COE explicaron que hay 49 casos activos Coronavirus en Catamarca: confirmaron dos contagios nuevos y tres pacientes recuperados Con el retorno a la fase 5, hubo gran cantidad de gente en las calles de la ciud Foto: César Gómez / La Unión 20 Julio de 2020 21.05\\nInformación oficial actualizada sobre COVID19\\nEl COE para la prevención de Coronavirus y Dengue actualizó la información oficial sobre la situación epidemiológica provincial y los trabajos de prevención y contención que se desarrollan en todo el territorio de Catamarca en el marco de la lucha contra el Dengue y el COVID-19.\\nCOVID-19\\nHasta las 21 horas del lunes 20 de julio, se han detectado 2 nuevos casos positivos de coronavirus en la provincia de Catamarca. El total acumulado de casos positivos detectados asciende a 60.\\nDesde el COE informaron que hay 3 nuevos pacientes recuperados. Por esta razón se contabilizan 60 casos positivos en total pero solamente 49 se consideran casos activos.\\nDatos generales hasta las 21 horas del 20 de julio\\nTotal de positivos: 60\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:53:25.008+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:53:25.008+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"3c9d4dd28a412b022d487749d28254bd30858d05\",\n" +
                "        \"url\": \"https://www.plusinformacion.com.ar/nota.php?Id=72869\",\n" +
                "        \"site_full\": \"www.plusinformacion.com.ar\",\n" +
                "        \"site\": \"plusinformacion.com.ar\",\n" +
                "        \"site_section\": \"http://www.plusinformacion.com.ar/sitemap_rss.php\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\"\n" +
                "        ],\n" +
                "        \"section_title\": \"PlusInformacion Noticias\",\n" +
                "        \"title\": \"Con 21 nuevos muertos, se monitorean \\\"actividades que más impactan\\\" en nuevos casos\",\n" +
                "        \"title_full\": \"Con 21 nuevos muertos, se monitorean \\\"actividades que más impactan\\\" en nuevos casos\",\n" +
                "        \"published\": \"2020-07-21T00:25:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 1,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://www.plusinformacion.com.ar/adjuntos/imagenes-notas/72869.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": null,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"3c9d4dd28a412b022d487749d28254bd30858d05\",\n" +
                "      \"url\": \"https://www.plusinformacion.com.ar/nota.php?Id=72869\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"PlusInformacion Noticias\",\n" +
                "      \"published\": \"2020-07-21T00:25:00.000+03:00\",\n" +
                "      \"title\": \"Con 21 nuevos muertos, se monitorean \\\"actividades que más impactan\\\" en nuevos casos\",\n" +
                "      \"text\": \"En el marco de reapertura decidido el viernes y siguiendo la experiencia de países que se encuentran más \\\"adelante\\\" en el tránsito de la pandemia, se está monitoreando \\\"qué actividades generan más impacto al aumento de número de casos\\\". El Ministerio de Salud informó esta mañana 21 nuevos fallecimientos por coronavirus en el país, lo que eleva a 2.281 la cifra de muertos desde marzo pasado, y se señaló que, en el marco de reapertura decidido el viernes y siguiendo la experiencia de países que se encuentran más \\\"adelante\\\" en el tránsito de la pandemia, se está monitoreando \\\"qué actividades generan más impacto al aumento de número de casos\\\". Así lo confirmó la secretaria de Acceso a la Salud, Carla Vizzotti, al encabezar el reporte diario que emite la autoridad sanitaria para dar cuenta de la situación epidemiológica en el país. La funcionaria explicó que, si se mira la experiencia internacional, se están \\\"viendo diferencias geográficas en los países que están desarrollando actividades\\\" y que lugares que están \\\"unas semanas adelante en la pandemia, están volviendo para atrás en algunas medidas\\\" de apertura. Según señaló Vizzotti, el mecanismo parece ser \\\"abrir, ver la evolución, y volver para atrás si hace falta\\\". Si bien en Argentina las 24 provincias tienen casos confirmados y no hay jurisdicción que no los haya tenido en los últimos 14 días, sigue existiendo una \\\"diferencia epidemiológica\\\" entre distritos con pocos positivos, aquellos que tienen \\\"brotes\\\" y los que tienen un número más importante de infectados. Producto de la migración interna y de la relajación de los cuidados, está habiendo casos \\\"en departamentos que no los habían tenido\\\" y en otros que los habían controlado, hay nuevas situaciones \\\"de transmisión por brote\\\". \\\"Hay áreas con transmisión sostenida, donde el esfuerzo tiene que aumentarse, como la provincia de Jujuy, el departamento de San Fernando en Chaco, el Área Metropolitana de Buenos Aires\\\", comentó Vizzotti. En ese sentido, dijo que \\\"cuando se baja la guardia, se piensa que ya no va a haber casos, empieza a haber reuniones sin las prevenciones que se recomiendan y que ingrese (allí) una persona con el virus, realmente es una situación de máximo riesgo\\\". La secretaria aclaró que, sin embargo, \\\"la aceleración del número de casos está en un aumento más lento\\\" al igual que el ingreso de pacientes a unidades de terapia intensiva. De las 68.561 personas que actualmente cursan la enfermedad en el país, 841 lo hacen en unidades de atención crítica y el 91% de estos últimos se encuentran ubicados en sanatorios del AMBA. El promedio nacional de ocupación de esas plazas -sin importar la causa de internación- es del 54,6% y, en el conglomerado que conforman la Ciudad y los distritos bonaerenses que la circundan, aumenta a 65% \\\"con mayor tensión\\\" en la capital, especialmente en clínicas privadas. Desde el último reporte emitido se registró la muerte de 17 varones y cuatro mujeres, con edades que oscilan entre 51 y 87 años, cuyas residencias se distribuían entre la Provincia de Buenos Aires (10 casos), Ciudad de Buenos Aires (9 casos), Chaco y Río Negro (1 caso cada una). Así, la tasa de mortalidad se eleva a 49,8 personas cada millón de habitantes y el índice de letalidad es de 1,8% sobre los casos confirmados. Del total de 126.755 casos confirmados, 1.095 (0,9%) son \\\"importados\\\", 40.138 (31,7%) son contactos estrechos de casos confirmados, 63.648 (50,2%) son casos de circulación comunitaria y el resto se encuentra en investigación epidemiológica. El índice que mide la cantidad de positivos sobre test realizados tiene un promedio total desde el inicio de la pandemia de 27,6%, ayer fue de 44,8 a nivel nacional, mientras que en la Ciudad fue de 47,9 y de 51,8 en la Provincia de Buenos Aires. Ayer fueron realizadas 11.068 nuevas muestras y desde el inicio del brote se realizaron 552.306 pruebas diagnósticas para esta enfermedad, lo que equivale a 12.171,6 muestras por millón de habitantes. \",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:56:04.020+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:56:04.020+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"5fd832ef103ce625a523a09606c76547c0e71a9f\",\n" +
                "        \"url\": \"https://el-periodico.com.ar/contenido/104837/coronavirus-en-cordoba-se-confirmaron-39-casos-este-lunes-en-la-provincia\",\n" +
                "        \"site_full\": \"el-periodico.com.ar\",\n" +
                "        \"site\": \"el-periodico.com.ar\",\n" +
                "        \"site_section\": \"http://el-periodico.com.ar/rss/social.xml\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\",\n" +
                "          \"tennis\",\n" +
                "          \"sports\"\n" +
                "        ],\n" +
                "        \"section_title\": \"El Periódico - San Francisco Córdoba Argentina\",\n" +
                "        \"title\": \"Coronavirus en Córdoba: se confirmaron 39 casos este lunes en la provincia\",\n" +
                "        \"title_full\": \"Coronavirus en Córdoba: se confirmaron 39 casos este lunes en la provincia\",\n" +
                "        \"published\": \"2020-07-20T23:52:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 0,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0,\n" +
                "        \"main_image\": \"https://el-periodico.com.ar/download/multimedia.normal.91ef993311e6da24.636f726f6e6176697275735f6e6f726d616c2e6a7067.jpg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": null,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"5fd832ef103ce625a523a09606c76547c0e71a9f\",\n" +
                "      \"url\": \"https://el-periodico.com.ar/contenido/104837/coronavirus-en-cordoba-se-confirmaron-39-casos-este-lunes-en-la-provincia\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"\",\n" +
                "      \"published\": \"2020-07-20T23:52:00.000+03:00\",\n" +
                "      \"title\": \"Coronavirus en Córdoba: se confirmaron 39 casos este lunes en la provincia\",\n" +
                "      \"text\": \"Coronavirus en Córdoba: se confirmaron 39 casos este lunes en la provincia Local 20 de julio de 2020 Del total de casos, 34 están vinculados a contactos estrechos de casos confirmados anteriormente y cinco se encuentran en investigación: dos en Villa María y tres en Córdoba Capital.\\nEl Ministerio de Salud de la Provincia informó que en el día de hoy se confirmaron 39 nuevos casos de Covid-19, de los cuales 16 tienen domicilio en la ciudad de Córdoba (Capital), tres en Marcos Juárez (Marcos Juárez), dos en Inriville (Marcos Juárez), 9 en Oliva (Tercero Arriba), dos en Villa María (General San Martín), tres en Villa Dolores (San Javier) y cuatro en Oncativo (Río Segundo).\\nDel total de casos, 34 están vinculados a contactos estrechos de casos confirmados anteriormente y cinco se encuentran en investigación (dos en Villa María y tres en Córdoba Capital).\\nAfortunadamente esta jornada no hubo que lamentar fallecimientos por coronavirus.\\nSan Francisco y en el departamento San Justo siguen sin registrar casos en el último tiempo. Hasta hoy el departamento lleva registrados 16 casos en total: cinco en San Francisco, cuatro en Brinkmann, tres en Morteros, dos en Arroyito y dos en Marull.\\nDesde el inicio de la pandemia hasta la fecha, en la provincia de Córdoba se realizaron testeos con PCR (hisopados) a 104.564 personas. Esto resulta en una tasa de 27.806 personas estudiadas con PCR por cada 1 millón de habitantes. Hoy se efectuaron 2.099 estudios, de los cuales 2.059 son PCR y 40 test serológicos.\\nSe mantiene para la ciudad de Córdoba la fase de transmisión comunitaria, según constató la cartera sanitaria.\\nVer tablero de situación en Córdoba El detalle en Córdoba\\nDesde el comienzo de la pandemia, en la provincia de Córdoba se notificaron 29.055 casos, de los cuales 26.420 se descartaron y 1.314 se confirmaron.\\nDe esas 1.314 personas, 603 (45,89%) se encuentran en tratamiento ambulatorio con aislamiento domiciliario; 134 (10,19%) en tratamiento ambulatorio en instituciones intermedias; 57 (4,33%) en tratamiento hospitalario (internados), 40 (3,04%) fallecieron y 13 (0,99%) se encuentran en investigación porque corresponden a casos con residencia en otras provincias y en Brasil, de quienes no se cuenta información sobre su estado clínico.\\nEn relación con las personas recuperadas (alta), si no consideramos los casos confirmados en los últimos 15 días, la proporción de altas es de 66%. Operativos identificar\\nCon el objetivo de desarrollar estrategias de búsqueda activa de casos sospechosos de Covid-19, continúan los Operativos del Programa Identificar.\\nHoy se llevaron a cabo en las localidades de Villa Dolores (San Javier), Marcos Juárez (Marcos Juárez), Oliva (Tercero Arriba), Malvinas Argentinas (Colón) y en los barrios Campo de la Rivera (Capital), Villa Inés (Capital) y Zumarán (Capital). Durante esta estrategia sanitariase realizaron en total 760 estudios, de los cuales 720 corresponden a toma de muestra con hisopados (PCR) a quienes tuvieron contacto estrecho con casos positivos o presentaban síntomas, y 40 a test serológicos en el marco de un muestreo poblacional en esas zonas. Las muestras de los hisopados están siendo procesadas por el Laboratorio Central de la Provincia.\\nCabe aclarar que se encuentra en investigación la actuación de un profesional del servicio de enfermería debido a que una persona miembro del equipo de salud de dicha institución tuvo contacto estrecho con un caso positivo, luego de saber sobre dicha confirmación e incumpliendo ambos las correspondientes medidas el aislamiento.\\n*Cabe aclarar que el total de casos de Covid-19 para la provincia de Córdoba (1314) registra una diferencia con lo publicado por el Ministerio de Salud de la Nación (1319) debido a que:\\nNación no descontó aún del total acumulado de Córdoba, el caso de Río Cuarto que se había notificado desde un laboratorio de CABA, pero que luego se descartó en Córdoba por medio del Laboratorio Central de la Provincia. Para nuestro análisis, se consideran tres casos de personas con domicilio en otras provincias (San Luis, Santa Fe y Mendoza), que fueron diagnosticadas y asistidas en Córdoba hasta su recuperación. La provincia de Córdoba cuenta esos casos en su total acumulado, pero Nación no. Nación no descontó aún el caso notificado que se computó dos veces por error. Además de las diferencias explicadas en los puntos anteriores, hoy Nación publica más casos en el total acumulado de la provincia de Córdoba por causas que ambas carteras sanitarias se encuentran investigando, y que pueden estar relacionadas con las fallas del Sistema Nacional de Vigilancia de la Salud (SNVS) registradas en los últimos días.\\nTeléfono de atención COE\\nEl número de teléfono 0800 122 1444 para COVID-19, habilita la nueva opción 5 específica para el Centro de Operaciones de Emergencia (COE).\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [\n" +
                "        \"https://coe-reporte-diario-mapasydatos.hub.arcgis.com/\",\n" +
                "        \"https://www.coe-reporte-diario-mapasydatos.hub.arcgis.com/\",\n" +
                "        \"https://coe-reporte-diario-mapasydatos.hub.arcgis.com\"\n" +
                "      ],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T04:59:00.002+03:00\",\n" +
                "      \"updated\": \"2020-07-21T04:59:00.002+03:00\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"thread\": {\n" +
                "        \"uuid\": \"66c85da8df22928ef255c8129a9c38722a32ad70\",\n" +
                "        \"url\": \"https://www.cronica.com.ar/info-general/Coronel-Suarez-registro-sus-primeros-dos-casos-de-coronavirus-20200720-0039.html\",\n" +
                "        \"site_full\": \"www.cronica.com.ar\",\n" +
                "        \"site\": \"cronica.com.ar\",\n" +
                "        \"site_section\": \"https://www.cronica.com.ar/\",\n" +
                "        \"site_categories\": [\n" +
                "          \"media\",\n" +
                "          \"world_soccer\",\n" +
                "          \"sports\"\n" +
                "        ],\n" +
                "        \"section_title\": \"Crónica | Firme junto al pueblo\",\n" +
                "        \"title\": \"Coronel Suárez registró sus primeros dos casos de coronavirus\",\n" +
                "        \"title_full\": \"Coronel Suárez registró sus primeros dos casos de coronavirus | Crónica | Firme junto al pueblo\",\n" +
                "        \"published\": \"2020-07-20T21:31:00.000+03:00\",\n" +
                "        \"replies_count\": 0,\n" +
                "        \"participants_count\": 1,\n" +
                "        \"site_type\": \"news\",\n" +
                "        \"country\": \"AR\",\n" +
                "        \"spam_score\": 0.013,\n" +
                "        \"main_image\": \"https://www.cronica.com.ar/__export/1595269058349/sites/cronica/img/2020/07/20/ricardo_moccero_crop1595268797441.jpeg\",\n" +
                "        \"performance_score\": 0,\n" +
                "        \"domain_rank\": 12113,\n" +
                "        \"social\": {\n" +
                "          \"facebook\": {\n" +
                "            \"likes\": 0,\n" +
                "            \"comments\": 0,\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"gplus\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"pinterest\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"linkedin\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"stumbledupon\": {\n" +
                "            \"shares\": 0\n" +
                "          },\n" +
                "          \"vk\": {\n" +
                "            \"shares\": 0\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"uuid\": \"66c85da8df22928ef255c8129a9c38722a32ad70\",\n" +
                "      \"url\": \"https://www.cronica.com.ar/info-general/Coronel-Suarez-registro-sus-primeros-dos-casos-de-coronavirus-20200720-0039.html\",\n" +
                "      \"ord_in_thread\": 0,\n" +
                "      \"parent_url\": null,\n" +
                "      \"author\": \"Crónica\",\n" +
                "      \"published\": \"2020-07-20T21:31:00.000+03:00\",\n" +
                "      \"title\": \"Coronel Suárez registró sus primeros dos casos de coronavirus\",\n" +
                "      \"text\": \"Las autoridades del partido bonaerense de Coronel Suárez confirmaron los primeros dos casos positivos de coronavirus (orthocoronavirinae) tras 123 días del aislamiento social, preventivo y obligatorio.\\nSegún se indicó, se trata de dos personas de un grupo familiar que arribó hace 12 días de la Ciudad Autónoma de Buenos Aires.\\n\\\"No hay ningún cambio de fase, seguimos en 5, cuidándonos y respetándonos todos\\\" , aclaró hoy, no obstante, el intendente Ricado Moccero durante una conferencia de prensa.\\n.\\nEn ese marco, añadió: \\\"Está todo controlado, son dos casos confirmados que están en cuarentena, estamos en contacto permanente y esta gente ni bien tuvo síntomas se confirmó a través de telemedicina\\\".\\n\\\"No queremos que se genere ninguna paranoia, ninguna psicosis de nadie\\\", expresó en referencia a audios y mensajes difundidos por redes sociales.\\n.\\nMoccero pidió a los vecinos \\\"respeto por la familia que es de Suárez\\\" ya que \\\"a cualquiera le puede tocar y nadie está ajeno\\\" a la pandemia.\\nPor último se informó que \\\"en todo momento (los infectectados) permanecieron sin salir de su domicilio, ni para hacer compras y para hacerlo lo hacían en forma telefónica\\\".\",\n" +
                "      \"highlightText\": \"\",\n" +
                "      \"highlightTitle\": \"\",\n" +
                "      \"highlightThreadTitle\": \"\",\n" +
                "      \"language\": \"spanish\",\n" +
                "      \"external_links\": [],\n" +
                "      \"external_images\": [],\n" +
                "      \"entities\": {\n" +
                "        \"persons\": [],\n" +
                "        \"organizations\": [],\n" +
                "        \"locations\": []\n" +
                "      },\n" +
                "      \"rating\": null,\n" +
                "      \"crawled\": \"2020-07-21T05:09:36.000+03:00\",\n" +
                "      \"updated\": \"2020-07-21T05:09:36.000+03:00\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"totalResults\": 1846,\n" +
                "  \"moreResultsAvailable\": 1746,\n" +
                "  \"next\": \"/filterWebContent?token=86faad70-82e9-45cb-b381-d14614b31cbd&format=json&ts=1595315479005&q=coronavirus+casos+positivos++language%3Aspanish+thread.country%3AAR&sort=crawled\",\n" +
                "  \"requestsLeft\": 987,\n" +
                "  \"warnings\": null\n" +
                "}";
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }
}
